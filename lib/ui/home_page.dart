import 'dart:io';

import 'package:agenda_contatos/helpers/contact_helper.dart';
import 'package:agenda_contatos/ui/contact_page.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

//Criando enumerador para ordenar contatos
enum OrderOption{orderaz, orderza}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  ContactHelper helper = ContactHelper();
  List<Contact> contacts = List();




  @override
  void initState() {
    super.initState();
    //quando iniciar essa tela, todos os contatos serão carregados
    _listarTodos();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contatos"),
        backgroundColor: Colors.red,
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<OrderOption>(
            itemBuilder: (context) => <PopupMenuEntry<OrderOption>>[
              const PopupMenuItem <OrderOption>(
                child: Text("Ordenar de A-Z"),
                value: OrderOption.orderaz,
              ),
              const PopupMenuItem <OrderOption>(
                child: Text("Ordenar de Z-A"),
                value: OrderOption.orderza,
              ),
            ],
            onSelected: _orderList,
          )
        ],
      ),
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        //Função anônima que chama o método para chamar a tela de edição/salvar contato
          onPressed: (){
            _showContactPage();
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.red,
      ),
      body: Container(
        child: ListView.builder(
            itemCount: contacts.length,
            itemBuilder: (context, index){
             return _contactCard(context, index) ;
            }
        ),
      ),
    );
  }


  //card
  Widget _contactCard(BuildContext context, int index){
    return GestureDetector(
      child: Card(
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    //pega a imagem no banco
                      image: contacts[index].img != null ?
                          FileImage(File(contacts[index].img)) ://se não existir pega a imagem do asset
                          AssetImage("images/person.png")
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(contacts[index].nome ?? "",
                    style: TextStyle(fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                    ),
                    Text(contacts[index].email ?? "",
                      style: TextStyle(fontSize: 18.0,
                      ),
                    ),
                    Text(contacts[index].phone ?? "",
                      style: TextStyle(fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      onTap: (){
        _showOptions(context, index);
      },
    );
  }

  void _orderList(OrderOption result){
    switch(result){
      case OrderOption.orderaz:
        contacts.sort((a ,b){
         return a.nome.toLowerCase().compareTo(b.nome.toLowerCase());
        });
        break;
      case OrderOption.orderza:
        contacts.sort((a ,b){
         return b.nome.toLowerCase().compareTo(a.nome.toLowerCase());
        });
        break;
    }
    setState(() {

    });

  }

  void _showOptions(BuildContext context, int index){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return BottomSheet(
            onClosing: (){},
            builder: (context){
              return Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FlatButton(
                        child: Text("LIGAR",
                          style: TextStyle(color: Colors.green, fontSize: 20.0),
                        ),
                        onPressed: (){
                          launch("tel:${contacts[index].phone}");
                          Navigator.pop(context);
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FlatButton(
                        child: Text("EDITAR",
                          style: TextStyle(color: Colors.red, fontSize: 20.0),
                        ),
                        onPressed: (){
                          Navigator.pop(context);
                          //Ao clicar no botao do bottomsheet, ele vai chamar a próxima tela passando o contato do card
                          _showContactPage(contact: contacts[index]);
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: FlatButton(
                        child: Text("EXCLUIR",
                          style: TextStyle(color: Colors.red, fontSize: 20.0),
                        ),
                        onPressed: (){
                          helper.deleteContact(contacts[index].id);
                          setState(() {
                            contacts.removeAt(index);
                            Navigator.pop(context);
                          });
                        },
                      ),
                    ),
                  ],
                ),
              );
            },
          );
      }
    );
  }


  //Método para Chamar a tela de cadastro, com um parametro opcional(contato existente para ser editado) entre {}
  void _showContactPage({Contact contact}) async{
    //recContact vai esperar a resposta da outra pagina
    final recContact = await Navigator.push(context,
    //ContactPage(contact: contact,)) envia o contato de volta
    MaterialPageRoute(builder: (context)=> ContactPage(contact: contact,))
    );
    //verificar se está retornando algum contato, se o usuario voltar sem salvar
    if(recContact != null){
      //se retornar o contato, vai verificar se tinha sido enviado algum contato para editar
      if(contact != null){
        //atualizar
        await helper.updateContact(recContact);

      } else{
        //caso tenhamos um contato mas nenhum contato foi enviado
        await helper.saveContact(recContact);
      }
      _listarTodos();
    }
  }

  void _listarTodos(){
    helper.getAll().then((list){
      setState(() {
        contacts = list;
      });
    });
  }
}
