import 'dart:io';
import 'package:agenda_contatos/helpers/contact_helper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';



class ContactPage extends StatefulWidget {

  final Contact contact;
  ContactPage({this.contact});

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {

  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();

  final _nameFocus = FocusNode();
  bool _userEdited = false;
  Contact _editContact;

  @override
  void initState() {
    super.initState();
    //ao iniciar a tela
    if(widget.contact == null){
      //Se não houver o contato, ele será criado
      _editContact = Contact();
    }else{
      //Caso contrário ele pega o contato que essa página recebeu, transforma em mapa e cria um "novo contato" 
      _editContact = Contact.fromMap(widget.contact.toMap());

      //colocando valores do contato editado nos textfield
      //controllers irão receber os dados do mapa
      _nameController.text = _editContact.nome;
      _emailController.text = _editContact.email;
      _phoneController.text = _editContact.phone;
    }
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          //Se o contato já tiver nome ele aparece o nome na appbar caso contrário aparece "Novo Contato"
          title: Text(_editContact.nome ?? "Novo Contato"),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed:(){
            if(_editContact.nome!=null &&_editContact.nome.isNotEmpty){
              //Navigator trabalha com um esquema de pilha
              Navigator.pop(context, _editContact);
            }else{
              //se estiver vazio o campo nome e o usuario tentar salvar, o campo nome será focado
              FocusScope.of(context).requestFocus(_nameFocus);
            }
          },
          child: Icon(Icons.save),
          backgroundColor: Colors.red,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              GestureDetector(
                child:  Container(
                  width: 140.0,
                  height: 140.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      //pega a imagem no banco
                        image: _editContact.img != null ?
                        FileImage(File(_editContact.img)) ://se não existir pega a imagem do asset
                        AssetImage("images/person.png")
                    ),
                  ),
                ),
                onTap: (){
                  ImagePicker.pickImage(source: ImageSource.camera).then((file){
                      if(file==null) return;
                      setState(() {
                        _editContact.img = file.path;
                      });
                  });
                },
              ),
              TextField(
                controller: _nameController,
                focusNode: _nameFocus,
                decoration: InputDecoration(labelText: "Nome"),
                onChanged: (text){
                  _userEdited = true;
                  setState(() {
                    _editContact.nome = text;
                  });
                },
              ),
              TextField(
                controller: _emailController,
                decoration: InputDecoration(labelText: "E-mail"),
                onChanged: (text){
                  _userEdited = true;
                  _editContact.email = text;
                },
                keyboardType: TextInputType.emailAddress,
              ),
              TextField(
                controller: _phoneController,
                decoration: InputDecoration(labelText: "Telefone"),
                onChanged: (text){
                  _userEdited = true;
                  _editContact.phone = text;
                },
                keyboardType: TextInputType.phone,
              ),
            ],
          ) ,
        ),
      ),
    );
  }



  Future<bool>_requestPop(){
    if(_userEdited){
      showDialog(context: context,
      builder: (context){
        return AlertDialog(
          title: Text("Descartar Alterações"),
          content: Text("Se sair serão perdidas"),
          actions: <Widget>[
            FlatButton(
              child: Text("CANCELAR"),
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("SIM"),
              onPressed: (){
                //fechar alert
                Navigator.pop(context);
                //fechar tela
                Navigator.pop(context);
              },
            ),
          ],
        );
      });
      return Future.value(false);
    }else{
      return Future.value(true);
    }
  }
}
