import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final String tabela = "Contatos";
final String idColumn = "idColumn";
final String nomeColumn = "nomeColumn";
final String emailColumn = "emailColumn";
final String phoneColumn = "phoneColumn";
final String imgColumn = "imgColumn";


class ContactHelper{
  //objeto da mesma classe, com uma instância só(singleton)
  static final ContactHelper _instance = ContactHelper.internal();//construtor interno
  factory ContactHelper() => _instance;
  ContactHelper.internal();

  Database _db;
  Future<Database> get db async {
    if(_db != null){
      return _db;
    } else {
      _db = await initDb();
      return _db;
    }
  }

  //Criar tabela
  Future<Database> initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "contactsnew.db");

    return await openDatabase(path, version: 1, onCreate:(Database db, int newerVersion) async{
      await db.execute(
        "CREATE TABLE $tabela($idColumn INTEGER PRIMARY KEY, $nomeColumn TEXT, $emailColumn TEXT,"
            "$phoneColumn TEXT, $imgColumn TEXT)"

      );
    });
  }
  //Insert
  Future<Contact>saveContact(Contact contact) async{
    Database dbContact = await db;
    contact.id = await dbContact.insert(tabela, contact.toMap());
    return contact;
  }

  //Select
  Future<Contact>getContact(int id)async{
    Database dbContact = await db;
    List<Map> maps = await dbContact.query(tabela,
    columns: [idColumn, nomeColumn, emailColumn, phoneColumn, imgColumn],
    where: "$idColumn = ?",
    whereArgs: [id]);
    //Se for maior que zero é pq existe o dado
    if(maps.length>0){
      //retorna o primeiro pois é um select com id, então só tem um
      return Contact.fromMap(maps.first);
    }else{
      return null;
    }
  }

  //delete
  Future<int>deleteContact(int id) async{
    Database dbContact = await db;
    return await dbContact.delete(tabela, where: "$idColumn = ?", whereArgs: [id]);
  }

  //Update
  Future<int>updateContact(Contact contact) async{
    Database dbContact = await db;
    return await dbContact.update(tabela, contact.toMap(),where: "$idColumn = ?", whereArgs: [contact.id]);
  }

  //Select all
    Future<List>getAll() async{
    Database dbContact = await db;
    List listMap = await dbContact.rawQuery("SELECT * FROM $tabela");
    List<Contact>listContact = List();
    for(Map m in listMap){
      listContact.add(Contact.fromMap(m));
    }
    return listContact;
  }

  //Pegar Total de registros
  Future<int> getNumber()async{
    Database dbContact = await db;
    return Sqflite.firstIntValue(await dbContact.rawQuery("SELECT COUNT(*) FROM $tabela"));
  }

  Future close() async{
    Database dbContact = await db;
    dbContact.close();
  }

}

class Contact{

  int id;
  String nome;
  String email;
  String phone;
  String img;

  Contact();

  Contact.fromMap(Map map){
    id = map[idColumn];
    nome = map[nomeColumn];
    email = map[emailColumn];
    phone = map[phoneColumn];
    img = map[imgColumn];
  }

  Map toMap(){
    Map<String, dynamic>map = {
      nomeColumn: nome,
      emailColumn: email,
      phoneColumn: phone,
      imgColumn: img,
    };
    if(id != null){
      map[idColumn] =id;
    }
    return map;
  }
  @override
  String toString() {
    return "Contact(id: $id, name: $nome, email: $email, phone: $phone, img: $img)";
  }


}